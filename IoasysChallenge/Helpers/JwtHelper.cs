﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;

namespace IoasysChallenge.Helpers
{
    public class JwtHelper
    {
        public string Audience { get; }
        public string Issuer { get; }
        public int ValidForMinutes { get; }
        public int RefreshTokenValidForMinutes { get; }
        public SigningCredentials SigningCredentials { get; }

        public DateTime IssuedAt => DateTime.UtcNow;
        public DateTime NotBefore => IssuedAt;
        public DateTime AccessTokenExpiration => IssuedAt.AddMinutes(ValidForMinutes);
        public DateTime RefreshTokenExpiration => IssuedAt.AddMinutes(RefreshTokenValidForMinutes);

        public JwtHelper(IConfiguration configuration)
        {
            Issuer = configuration["JwtHelper:Issuer"];
            Audience = configuration["JwtHelper:Audience"];
            ValidForMinutes = Convert.ToInt32(configuration["JwtHelper:ValidForMinutes"]);
            RefreshTokenValidForMinutes = Convert.ToInt32(configuration["JwtHelper:RefreshTokenValidForMinutes"]);
        }
    }
}
