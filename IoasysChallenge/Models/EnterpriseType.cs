﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysChallenge.Models
{
    public class EnterpriseType : BaseEntity
    {
        public string Enterprise_Type_Name { get; set; }

        public virtual ICollection<Enterprise> Enterprise { get; set; }
    }
}
