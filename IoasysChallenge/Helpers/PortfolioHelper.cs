﻿using IoasysChallenge.Models;
using System.Collections.Generic;

namespace IoasysChallenge.Helpers
{
    public class PortfolioHelper
    {
        public int EnterpriseNumber { get; set; }

        public List<Enterprise> Enterprises { get; set; }
    }
}
