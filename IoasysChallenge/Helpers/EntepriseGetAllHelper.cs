﻿using IoasysChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysChallenge.Helpers
{
    public class EntepriseGetAllHelper
    {

        public int Id { get; set; }
        public string Email_Enterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool Own_Enterprise { get; set; }
        public string Enterprise_Name { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Value { get; set; }
        public decimal Share_Price { get; set; }

        public EnterpriseTypeHelper Enterprise_Type { get; set; }
    }
}
