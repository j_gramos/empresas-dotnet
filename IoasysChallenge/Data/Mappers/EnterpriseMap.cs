﻿using IoasysChallenge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IoasysChallenge.Data.Mappers
{
    public class EnterpriseMap : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.ToTable("tb_enterprise");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id);
            builder.Property(e => e.Email_Enterprise).HasMaxLength(50);
            builder.Property(e => e.Facebook).HasMaxLength(50);
            builder.Property(e => e.Twitter).HasMaxLength(50);
            builder.Property(e => e.Linkedin).HasMaxLength(50);
            builder.Property(e => e.Phone).HasMaxLength(50);
            builder.Property(e => e.Own_Enterprise);
            builder.Property(e => e.Enterprise_Name).HasMaxLength(50);
            builder.Property(e => e.Photo);
            builder.Property(e => e.Description);
            builder.Property(e => e.City).HasMaxLength(50);
            builder.Property(e => e.Country).HasMaxLength(50);
            builder.Property(e => e.Value).HasColumnType("decimal");
            builder.Property(e => e.Share_Price).HasColumnType("decimal(18,1)");
            builder.Property(e => e.Enterprise_Type_Id);
            builder.Property(e => e.Shares);
            builder.Property(e => e.Own_Shares);

            builder.HasOne(e => e.Portfolio).WithMany(e => e.Enterprises);
            builder.HasOne(e => e.EnterpriseType).WithMany(e => e.Enterprise).HasForeignKey(e => e.Enterprise_Type_Id);


        }
    }
}
