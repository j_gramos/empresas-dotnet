﻿using IoasysChallenge.Helpers;
using System;

namespace IoasysChallenge.Interfaces
{
    public interface IUserService
    {
        UserHelper Authenticate(string login, string password);
        string GetEnterprise(int id);
        string GenerateToken(string email);
        Tuple<string,string> GetToken();


    }
}
