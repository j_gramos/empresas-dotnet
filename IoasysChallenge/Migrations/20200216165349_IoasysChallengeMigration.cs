﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IoasysChallenge.Migrations
{
    public partial class IoasysChallengeMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tb_enterpriseType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Enterprise_Type_Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_enterpriseType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tb_investor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Investor_Name = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Password = table.Column<string>(maxLength: 50, nullable: true),
                    City = table.Column<string>(maxLength: 50, nullable: true),
                    Country = table.Column<string>(maxLength: 50, nullable: true),
                    Balance = table.Column<decimal>(type: "decimal(18,1)", nullable: false),
                    Photo = table.Column<string>(nullable: true),
                    Portfolio_Value = table.Column<decimal>(type: "decimal(18,1)", nullable: false),
                    First_Access = table.Column<bool>(nullable: false),
                    Super_Angel = table.Column<bool>(nullable: false),
                    IdEnterprise = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_investor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tb_portfolio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdInvestor = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_portfolio", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tb_portfolio_tb_investor_IdInvestor",
                        column: x => x.IdInvestor,
                        principalTable: "tb_investor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tb_enterprise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email_Enterprise = table.Column<string>(maxLength: 50, nullable: true),
                    Facebook = table.Column<string>(maxLength: 50, nullable: true),
                    Twitter = table.Column<string>(maxLength: 50, nullable: true),
                    Linkedin = table.Column<string>(maxLength: 50, nullable: true),
                    Phone = table.Column<string>(maxLength: 50, nullable: true),
                    Own_Enterprise = table.Column<bool>(nullable: false),
                    Enterprise_Name = table.Column<string>(maxLength: 50, nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    City = table.Column<string>(maxLength: 50, nullable: true),
                    Country = table.Column<string>(maxLength: 50, nullable: true),
                    Value = table.Column<decimal>(type: "decimal", nullable: false),
                    Share_Price = table.Column<decimal>(type: "decimal(18,1)", nullable: false),
                    Enterprise_Type_Id = table.Column<int>(nullable: false),
                    Shares = table.Column<int>(nullable: false),
                    Own_Shares = table.Column<int>(nullable: false),
                    PortfolioId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_enterprise", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tb_enterprise_tb_enterpriseType_Enterprise_Type_Id",
                        column: x => x.Enterprise_Type_Id,
                        principalTable: "tb_enterpriseType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tb_enterprise_tb_portfolio_PortfolioId",
                        column: x => x.PortfolioId,
                        principalTable: "tb_portfolio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tb_enterprise_Enterprise_Type_Id",
                table: "tb_enterprise",
                column: "Enterprise_Type_Id");

            migrationBuilder.CreateIndex(
                name: "IX_tb_enterprise_PortfolioId",
                table: "tb_enterprise",
                column: "PortfolioId");

            migrationBuilder.CreateIndex(
                name: "IX_tb_portfolio_IdInvestor",
                table: "tb_portfolio",
                column: "IdInvestor",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tb_enterprise");

            migrationBuilder.DropTable(
                name: "tb_enterpriseType");

            migrationBuilder.DropTable(
                name: "tb_portfolio");

            migrationBuilder.DropTable(
                name: "tb_investor");
        }
    }
}
