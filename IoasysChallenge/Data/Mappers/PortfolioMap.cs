﻿using IoasysChallenge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IoasysChallenge.Data.Mappers
{
    public class PortfolioMap : IEntityTypeConfiguration<Portfolio>
    {
        public void Configure(EntityTypeBuilder<Portfolio> builder)
        {
            builder.ToTable("tb_portfolio");

            builder.HasKey(p => p.Id);
            builder.Property(p => p.IdInvestor);

            builder.HasOne(p => p.Investor).WithOne(p => p.Portfolio).HasForeignKey<Portfolio>(p => p.IdInvestor);
        }
    }
}
