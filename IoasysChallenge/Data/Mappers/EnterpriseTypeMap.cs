﻿using IoasysChallenge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IoasysChallenge.Data.Mappers
{
    public class EnterpriseTypeMap : IEntityTypeConfiguration<EnterpriseType>
    {
        public void Configure(EntityTypeBuilder<EnterpriseType> builder)
        {
            builder.ToTable("tb_enterpriseType");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id);
            builder.Property(e => e.Enterprise_Type_Name);
        }
    }
}
