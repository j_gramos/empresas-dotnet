﻿using IoasysChallenge.Interfaces;
using IoasysChallenge.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IoasysChallenge.Controllers
{

    [ApiController]
    [Route("api/v1/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("auth/sign_in")]
        public ActionResult Authenticate([FromBody] User user)
        {
            var investor = userService.Authenticate(user.Email, user.Password);

            if (investor == null)
            {

                return Unauthorized(
                    new
                    {
                        success = false,
                        errors = "Invalid login credentials. Please try again."
                    });
            }

            userService.GenerateToken(investor.Email);
            var tokenHelper = userService.GetToken();
            Response.Headers.Add("access-token", tokenHelper.Item2);
            Response.Headers.Add("token-type", "Bearer");
            Response.Headers.Add("client", tokenHelper.Item1);
            Response.Headers.Add("uid", user.Email);

            var enterpriseInvestor = userService.GetEnterprise(investor.Id);
            return Ok(new
            {
                investor,
                enterprise = enterpriseInvestor == null ? enterpriseInvestor : null,
                success = true
            });


        }

    }
}
