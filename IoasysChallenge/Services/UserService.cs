﻿using IoasysChallenge.Helpers;
using IoasysChallenge.Interfaces;
using IoasysChallenge.Interfaces.Repository;
using IoasysChallenge.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace IoasysChallenge.Services
{
    public class UserService : IUserService
    {

        private readonly IRepository<Investor> repository;
        private readonly AppSettings appSettings;
        private readonly JwtHelper settings;
        public UserHelper Investor { get; private set; }
        public PortfolioHelper Portfolio { get; private set; }

        private Tuple<string,string> Token;


        public UserService(IOptions<AppSettings> appSettings, IRepository<Investor> repository, JwtHelper settings)
        {
            this.appSettings = appSettings.Value;
            this.repository = repository;
            this.settings = settings;
        }


        public UserHelper Authenticate(string email, string password)
        {
            var user = repository.Select(i => i.Email.Equals(email) && i.Password.Equals(password)).FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            var result = GetInvestor(user.Id);
            return result;
        }

        public string GenerateToken(string email)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Email, email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),

                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                NotBefore = DateTime.Now,
                Issuer = settings.Issuer,
                Audience = settings.Audience,
                IssuedAt = settings.IssuedAt

            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var resultToken = tokenHandler.WriteToken(token);
            Token = new Tuple<string, string>(token.Id, resultToken);
            return resultToken;
        }


        public UserHelper GetInvestor(int id)
        {
            List<Enterprise> listOfEnterprises = new List<Enterprise>();

            var investor = repository.Select(i => i.Id == id).FirstOrDefault();

            this.Investor = new UserHelper
            {
                Id = investor.Id,
                Investor_Name = investor.Investor_Name,
                Email = investor.Email,
                City = investor.City,
                Country = investor.Country,
                Balance = investor.Balance,
                Photo = investor.Photo,

                Portfolio = new PortfolioHelper
                {
                    EnterpriseNumber = listOfEnterprises.Any() ? listOfEnterprises.Count : 0,
                    Enterprises = listOfEnterprises.Any() ? listOfEnterprises.Select(e => new Enterprise
                    {
                        Email_Enterprise = e.Email_Enterprise,
                        Facebook = e.Facebook,
                        Twitter = e.Twitter,
                        Linkedin = e.Linkedin,
                        Phone = e.Phone,
                        Own_Enterprise = e.Own_Enterprise,
                        Enterprise_Name = e.Enterprise_Name,
                        Photo = e.Photo,
                        Description = e.Description,
                        City = e.City,
                        Country = e.Country,
                        Value = e.Value,
                        Share_Price = e.Share_Price,
                        Enterprise_Type_Id = e.Enterprise_Type_Id,
                        Shares = e.Shares,
                        Own_Shares = e.Own_Shares
                    }).ToList() : listOfEnterprises.ToList()
                },
                Portfolio_Value = investor.Portfolio_Value,
                First_Access = investor.First_Access,
                Super_Angel = investor.Super_Angel
            };

            return Investor;
        }

        public string GetEnterprise(int id)
        {
            var investor = repository.Select(i => i.Id == id).FirstOrDefault();
            return investor.IdEnterprise.ToString();
        }

        public Tuple<string,string> GetToken()
        {
            return Token;
        }
    }
}