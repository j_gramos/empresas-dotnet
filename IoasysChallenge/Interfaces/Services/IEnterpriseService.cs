﻿using IoasysChallenge.Helpers;
using IoasysChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysChallenge.Interfaces.Services
{
    public interface IEnterpriseService
    {
        EnterpriseByIdHelper Get(int id);
        List<EnterpriseByTypeAndNameHelper> GetAllByTypeName(string enterpriseType, string name);

        List<EntepriseGetAllHelper> GetAll();
   
    }
}
