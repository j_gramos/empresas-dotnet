﻿using IoasysChallenge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IoasysChallenge.Data.Mappers
{
    public class InvestorMap : IEntityTypeConfiguration<Investor>
    {
        public void Configure(EntityTypeBuilder<Investor> builder)
        {
            builder.ToTable("tb_investor");

            builder.HasKey(i => i.Id);

            builder.Property(i => i.Investor_Name).HasMaxLength(50);
            builder.Property(i => i.Email).HasMaxLength(50);
            builder.Property(i => i.Password).HasMaxLength(50);
            builder.Property(i => i.City).HasMaxLength(50);
            builder.Property(i => i.Country).HasMaxLength(50);
            builder.Property(i => i.Balance).HasColumnType("decimal(18,1)");
            builder.Property(i => i.Photo);
            builder.Property(i => i.Portfolio_Value).HasColumnType("decimal(18,1)");
            builder.Property(i => i.First_Access);
            builder.Property(i => i.Super_Angel);
            builder.Property(i => i.IdEnterprise).IsRequired(false);
        }
    }
}
