﻿using IoasysChallenge.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace IoasysChallenge.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]

    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseService enterpriseService;
        public EnterprisesController(IEnterpriseService enterpriseService)
        {
            this.enterpriseService = enterpriseService;
        }


        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var result = enterpriseService.Get(id);

            if (result == null)
            {
                return NotFound(new
                {
                    status = "404",
                    error = "Not Found"
                });
            }
            else
            {
                return Ok(new
                {
                    enterprise = result,
                    success = true
                });
            }
        }


        [HttpGet()]
        public ActionResult Get([FromQuery]string enterprise_Types, [FromQuery]string name)
        {
           
            if (enterprise_Types == null && name == null)
            {
                return Ok(new { enterprises= enterpriseService.GetAll() });
            }
            var result = enterpriseService.GetAllByTypeName(enterprise_Types, name);
            return Ok(new
            {
                enterprises = result
            });
        }

    }
}
