﻿using IoasysChallenge.Helpers;
using IoasysChallenge.Interfaces.Repository;
using IoasysChallenge.Interfaces.Services;
using IoasysChallenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace IoasysChallenge.Services
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly IRepository<Enterprise> repository;
        private readonly IRepository<EnterpriseType> repEnterpriseType;
        public EnterpriseByIdHelper Enterprise { get; private set; }
        public EnterpriseByTypeAndNameHelper Enterprises { get; private set; }


        public EnterpriseService(IRepository<Enterprise> repository, IRepository<EnterpriseType> repEnterpriseType)
        {
            this.repository = repository;
            this.repEnterpriseType = repEnterpriseType;

        }
        public EnterpriseByIdHelper Get(int id)
        {
            var result = repository.Get(id);
            if (result == null)
            {
                return Enterprise;
            }
            this.Enterprise = new EnterpriseByIdHelper();
            BuildEnterprise(Enterprise, result);
            return Enterprise;
        }

        public List<EnterpriseByTypeAndNameHelper> GetAllByTypeName(string enterpriseTypeId, string name)
        {
            var enterprises = repository.Select(e => e.Enterprise_Type_Id == int.Parse(enterpriseTypeId) && e.Enterprise_Name.Contains(name));
            var result = new List<EnterpriseByTypeAndNameHelper>();
            foreach (var enterprise in enterprises)
            {
                var enterpriseType = repEnterpriseType.Get(enterprise.Enterprise_Type_Id);
                result.Add(
                new EnterpriseByTypeAndNameHelper
                {
                    Id = enterprise.Id,
                    Email_Enterprise = enterprise.Email_Enterprise.Any() ? enterprise.Email_Enterprise : null,
                    Facebook = enterprise.Facebook.Any() ? enterprise.Facebook : null,
                    Twitter = enterprise.Twitter.Any() ? enterprise.Twitter : null,
                    Linkedin = enterprise.Linkedin.Any() ? enterprise.Linkedin : null,
                    Phone = enterprise.Phone.Any() ? enterprise.Phone : null,
                    Own_Enterprise = enterprise.Own_Enterprise,
                    Enterprise_Name = enterprise.Enterprise_Name,
                    Photo = enterprise.Photo,
                    Description = enterprise.Description,
                    City = enterprise.City,
                    Country = enterprise.Country,
                    Value = enterprise.Value,
                    Share_Price = enterprise.Share_Price,
                    Enterprise_Type = new EnterpriseTypeHelper
                    {
                        Id = enterprise.Enterprise_Type_Id,
                        Enterprise_Type_Name = enterpriseType.Enterprise_Type_Name
                    }
                });
            }

            return result;
        }

        public void BuildEnterprise(EnterpriseByIdHelper enterpriseLogged, Enterprise enterprise)
        {

            enterpriseLogged.Id = enterprise.Id;
            enterpriseLogged.Enterprise_Name = enterprise.Enterprise_Name;
            enterpriseLogged.Description = enterprise.Description;
            enterpriseLogged.Email_Enterprise = enterprise.Email_Enterprise;
            enterpriseLogged.Facebook = enterprise.Facebook;
            enterpriseLogged.Twitter = enterprise.Twitter;
            enterpriseLogged.Linkedin = enterprise.Linkedin;
            enterpriseLogged.Phone = enterprise.Phone;
            enterpriseLogged.Own_Enterprise = enterprise.Own_Enterprise;
            enterpriseLogged.Photo = enterprise.Photo;
            enterpriseLogged.Value = enterprise.Value;
            enterpriseLogged.Shares = enterprise.Shares;
            enterpriseLogged.Share_Price = enterprise.Share_Price;
            enterpriseLogged.Own_Shares = enterprise.Own_Shares;
            enterpriseLogged.City = enterprise.City;
            enterpriseLogged.Country = enterprise.Country;
            var enterpriseType = repEnterpriseType.Get(enterprise.Enterprise_Type_Id);

            enterpriseLogged.Enterprise_Type = new EnterpriseTypeHelper
            {
                Id = enterprise.Enterprise_Type_Id,
                Enterprise_Type_Name = enterpriseType.Enterprise_Type_Name
            };
        }

        public List<EntepriseGetAllHelper> GetAll()
        {
            var enterprises = repository.FindAll();
            var getAll = new List<EntepriseGetAllHelper>();
            foreach (var enterprise in enterprises)
            {
                var enterpriseType = repEnterpriseType.Get(enterprise.Enterprise_Type_Id);
                getAll.Add(
                new EntepriseGetAllHelper
                {
                    Id = enterprise.Id,
                    Email_Enterprise = enterprise.Email_Enterprise,
                    Facebook = enterprise.Facebook,
                    Twitter = enterprise.Twitter,
                    Linkedin = enterprise.Linkedin,
                    Phone = enterprise.Phone,
                    Own_Enterprise = enterprise.Own_Enterprise,
                    Enterprise_Name = enterprise.Enterprise_Name,
                    Photo = enterprise.Photo,
                    Description = enterprise.Description,
                    City = enterprise.City,
                    Country = enterprise.Country,
                    Value = enterprise.Value,
                    Share_Price = enterprise.Share_Price,
                    Enterprise_Type = new EnterpriseTypeHelper
                    {
                        Id = enterprise.Enterprise_Type_Id,
                        Enterprise_Type_Name = enterpriseType.Enterprise_Type_Name
                    }
                });
            }

            return getAll;
        }
    }

}
