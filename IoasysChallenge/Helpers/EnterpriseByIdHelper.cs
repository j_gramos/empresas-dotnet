﻿using Newtonsoft.Json;

namespace IoasysChallenge.Helpers
{
    public class EnterpriseByIdHelper
    {
        public int Id { get; set; }
        [JsonProperty("enterprise_name")]
        public string Enterprise_Name { get; set; }
        public string Description { get; set; }
        [JsonProperty("email_enterprise")]
        public string Email_Enterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        [JsonProperty("own_enterprise")]
        public bool Own_Enterprise { get; set; }
        public string Photo { get; set; }
        public decimal Value { get; set; }
        public int Shares { get; set; }
        [JsonProperty("share_price")]
        public decimal Share_Price { get; set; }
        [JsonProperty("own_shares")]
        public int Own_Shares { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public EnterpriseTypeHelper Enterprise_Type { get; set; }
    }
}
