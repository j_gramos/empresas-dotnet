﻿using IoasysChallenge.Data.Mappers;
using IoasysChallenge.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysChallenge.Data
{
    public class ChallengeContext : DbContext
    {
        public ChallengeContext() { }

        public ChallengeContext(DbContextOptions<ChallengeContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<EnterpriseType> EnterpriseType { get; set; }
        public DbSet<Investor> Investor { get; set; }
        public DbSet<Portfolio> Portfolio { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EnterpriseMap());
            modelBuilder.ApplyConfiguration(new EnterpriseTypeMap());
            modelBuilder.ApplyConfiguration(new InvestorMap());
            modelBuilder.ApplyConfiguration(new PortfolioMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}

