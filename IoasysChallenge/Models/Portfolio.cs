﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysChallenge.Models
{
    public class Portfolio : BaseEntity
    {
        public int IdInvestor { get; set; }

        public virtual Investor Investor { get; set; }
        public ICollection<Enterprise> Enterprises { get; set; }
    }
}
