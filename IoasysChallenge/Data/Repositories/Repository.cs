﻿using IoasysChallenge.Interfaces.Repository;
using IoasysChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IoasysChallenge.Data.Repositories
{
    public class Repository<Entitie> : IRepository<Entitie> where Entitie : BaseEntity
    {
        private readonly ChallengeContext context;

        public Repository(ChallengeContext context)
        {
            this.context = context;
        }

        public void Delete(Entitie entitie)
        {
           context.Remove(entitie);
        }

        public Entitie Get(int id)
        {
            return context.Find<Entitie>(id);
        }

        public Entitie Insert(Entitie entitie)
        {
            this.context.Set<Entitie>().Add(entitie);
            return entitie;
        }

        public List<Entitie> Select(Expression<Func<Entitie, bool>> predicate)
        {
            return this.context.Set<Entitie>().Where(predicate).ToList();

        }

        public List<Entitie> FindAll()
        {
            return this.context.Set<Entitie>().ToList();

        }

        public void Update(Entitie entitie)
        {
            this.context.Entry(entitie).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }
    }
}
