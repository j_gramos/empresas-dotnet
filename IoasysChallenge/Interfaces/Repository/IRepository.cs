﻿using IoasysChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IoasysChallenge.Interfaces.Repository
{
    public interface IRepository<Entitie> where Entitie : BaseEntity
    {
        Entitie Insert(Entitie entitie);
        void Update(Entitie entitie);
        List<Entitie> Select(Expression<Func<Entitie, bool>> predicate);
        Entitie Get(int id);
        void Delete(Entitie entitie);
        List<Entitie> FindAll();
    }
}
